<?php

return [
    "host_ip" => env("HOST_IP", "192.168.0.1"),
    "default_bridge_interface" => env("DEFAULT_BRIDGE_INTERFACE", "wlp0s20f3")
];
