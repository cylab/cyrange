<?php

return [
    "host" => env("VBOX_HOST", "127.0.0.1"),
    "user" => env("VBOX_USER", "vbox"),
    "password" => env("VBOX_PASSWORD", "vbox"),
    "root" => env("VBOX_ROOT", storage_path("app")),
    "guacamole" => env("GUACAMOLE_ENABLE", false)
];
