@extends('layouts.app')

@section('content')
<h1>Import DVD</h1>

<form method="POST" action="{{ route("dvds.import.do") }}">

    {{ csrf_field() }}

    <div class="mb-3">
        <label for="url" class="form-label">URL</label>

        <input id="url" type="url"
               class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}"
               name="url"
               placeholder="https://url.of/dvd.iso"
               value="{{ old('url') }}" required autofocus
               autocomplete="off">

        @if ($errors->has('url'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('url') }}</strong>
            </span>
        @endif
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">Name</label>

        <input id="name" type="text"
               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
               name="name"
               value="{{ old('name') }}" required>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>


    <div class="mb-3">
        <button type="submit" class="btn btn-primary">
            <i class="fas fa-cloud-download-alt"></i> Import
        </button>
    </div>
</form>

<script>
document.getElementById("url").addEventListener("keyup", (event) => {
    let url = event.target.value;
    let filename = url.substring(url.lastIndexOf('/') + 1);
    document.getElementById("name").value = filename;
});
</script>

@endsection
