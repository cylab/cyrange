@extends('layouts.app')

@section('title', "Disks")

@section('content')
<h1>DVDs</h1>

<div class="mb-4">
    <a href="{{ route("dvds.import")  }}"
        class="btn btn-primary"
        title="Download DVD image from URL">
           <i class="fas fa-cloud-download-alt"></i> Import DVD
    </a>
</div>

@foreach ($disks as $disk)
<div>
    <div>
        <a href="{{ route("dvds.show", ["dvd" => $disk->getUuid()]) }}"
           class="text-decoration-none">
            <b>{{ $disk->getName() }}</b>
        </a>
    </div>
    <div>
        {{ $disk->getSizeForHumans() }} / {{ $disk->getLogicalSizeForHumans() }}
    </div>
    <div>
        {{ $disk->isAttached() ? 'Attached to ' : '' }}
        @foreach ($disk->getMachines() as $vm)
        <a href="{{ \App\VM::findByUuid($vm->getUuid())->url() }}" class="badge badge-primary">{{ $vm->getName() }}</a>
        @endforeach
    </div>
</div>
@endforeach

@endsection
