@extends('layouts.app')

@section('content')
<h1>Deploy</h1>
<form method="POST" action="{{ action("ImageController@doDeploy") }}">

    @include("template.form")


    <div class="mb-3">
        <button type="submit" class="btn btn-primary">
            <i class="fas fa-cogs"></i> Deploy
        </button>
    </div>
</form>
@endsection

