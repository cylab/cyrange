@extends('layouts.app')

@section('title', 'Notifications')

@section('content')
<h1>Notifications</h1>

<p>
    <a href="{{ route('notifications.create') }}" class="btn btn-primary">
        <i class="fas fa-plus-circle"></i> New
    </a>
</p>

<table class="table table-lined">
    @foreach($notifications as $notification)
    <tr>
        <td>{{ $notification->created_at }}</td>
        <td>
            <a href="{{ route("notifications.show", ["notification" => $notification]) }}"
               class="text-decoration-none">
                {{ $notification->title }}
            </a>
        </td>
        <td>{{ $notification->user->name }}</td>
    </tr>
    @endforeach
</table>
@endsection
