@extends('layouts.app')

@section('title', $notification->title)

@section('content')
<div class="row justify-content-md-center">

    <div class="col-lg-8 bg-light p-3">
        <h1>{{ $notification->title }}</h1>
        <p class="text-muted">{{ $notification->created_at }}</p>

        {!! markdown($notification->markdown) !!}
    </div>
</div>
@endsection
