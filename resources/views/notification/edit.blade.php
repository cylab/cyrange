@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Notification</div>

    <div class="card-body">
        @if (!$notification->exists)
        <form method="POST" action="{{ route("notifications.store") }}">
        @else
        <form method="POST"
              action="{{ route("notifications.update", ["notification" => $notification]) }}">
            {{ method_field("PUT") }}
        @endif

            {{ csrf_field() }}

            <div class="mb-3">
                <label for="title" class="form-label">
                    Title
                </label>

                <input id="title" name="title"
                       type="text"
                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       value="{{ old('title', $notification->title) }}" required autofocus>

                @if ($errors->has('title'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="mb-3">
                <label for="markdown" class="form-label">
                    Message
                </label>

                <textarea id="markdown" name="markdown"
                       class="form-control{{ $errors->has('markdown') ? ' is-invalid' : '' }}"
                       rows="20"
                       placeholder="You can use **markdown**"
                       >{{ old('markdown', $notification->markdown) }}</textarea>

                @if ($errors->has('markdown'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('markdown') }}</strong>
                    </span>
                @endif
            </div>

            <div class="mb-3">
                <button type="submit" class="btn btn-primary"
                        title="Send notification to all users">
                    <i class="fas fa-paper-plane"></i> Save & send
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
