@extends('layouts.app')

@section('title', 'OTP')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card my-5">
                <div class="card-header">One Time Password</div>

                <div class="card-body">
                    <form method="POST" action="{{ action('Auth\OTPController@check') }}"
                          class="my-2">
                        @csrf
                        <input id="otp"
                               type="number" min="0" max="999999" step="1"
                               style="width: 20rem; display: inline-block;"
                               class="mr-1 form-control{{ $errors->has('otp') ? ' is-invalid' : '' }}"
                               autocomplete="off"
                               name="otp" value="" required autofocus>

                        @if ($errors->has('otp'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('otp') }}</strong>
                            </span>
                        @endif

                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
</div>
@endsection
