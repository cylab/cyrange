@extends('layouts.app')

@section('title', "Disks")

@section('content')
<h1>Disks</h1>

<div class="mb-4">
    <a class="btn btn-primary" href="{{ route('disks.create') }}">
        <i class="fas fa-plus-circle"></i> New
    </a>
</div>

@foreach ($disks as $disk)
<div>
    <div>

        <a href="{{ route("disks.show", ["disk" => $disk->getUuid()]) }}"
           class="text-decoration-none">
            <b>{{ $disk->getName() }}</b>
        </a>

    </div>
    <div>
        {{ $disk->getSizeForHumans() }} / {{ $disk->getLogicalSizeForHumans() }}
    </div>
    <div>
        {{ $disk->isAttached() ? 'Attached to ' : '' }}
        @foreach ($disk->getMachines() as $vm)
        <a href="{{ \App\VM::findByUuid($vm->getUuid())->url() }}" class="badge badge-primary">{{ $vm->getName() }}</a>
        @endforeach
    </div>
</div>
@endforeach

@endsection
