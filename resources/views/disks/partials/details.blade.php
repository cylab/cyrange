<p>{{ $disk->getLocation() }}</p>

<p>{{ $disk->getSizeForHumans() }} / {{ $disk->getLogicalSizeForHumans() }}</p>

<div>
    {{ $disk->isAttached() ? 'Attached to ' : '' }}
    @foreach ($disk->getMachines() as $vm)
    <a href="{{ \App\VM::findByUuid($vm->getUuid())->url() }}" class="text-decoration-none">{{ $vm->getName() }}</a>
    @endforeach
</div>