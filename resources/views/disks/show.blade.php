@extends('layouts.app')

@section('title', $disk->getName())

@section('content')
<h1>{{ $disk->getName() }}</h1>

<div class="mb-4">
    <form class="form-inline"
          action="{{ route('disks.destroy', ["disk" => $disk->getUuid()]) }}"
          method="post">
        @csrf
        @method("delete")
        <button class="btn btn-danger"
                title="Detach drive"><i class="fas fa-trash"></i>
            Destroy
        </button>
    </form>
</div>

@include("disks.partials.details")


@endsection
