@extends('layouts.app')

@section('content')
<h1>Storage</h1>

<form action="{{ route('vm.storage.store', ["uuid" => $uuid, "controller" => $controller]) }}" method="POST">
    @csrf
    @method("PUT")
    
    <div class="mb-3">
        <label class="form-label">Name</label>
        <input type="text" name="name" id="name" class="form-control"
               required autofocus>
    </div>
    
    <div class="mb-3">
        <label class="form-label">Size</label>
        
        <div class="input-group">
            <input type="int" name="size" id="size" class="form-control"
                   placeholder="100"
                   required>
            <span class="input-group-text">GB</span>
        </div>
    </div>
    
    <div class="mb-3">
        <button class="btn btn-primary" type="submit">Create</button>
    </div>
</form>
@endsection
