@extends('layouts.app')

@section('content')
<h1>Attach drive</h1>

<form action="{{ route('vm.storage.attach.dvd.do', ["uuid" => $uuid, "controller" => $controller]) }}" method="POST">
    @csrf

    <div class="mb-3">
        <label class="form-label">Drive</label>
        <select class="form-control" name="uuid">
            @foreach ($disks as $disk)
            <option value="{{ $disk->getUuid() }}">{{ $disk->getName() }}</option>
            @endforeach
        </select>

    </div>

    <div class="mb-3">
        <button class="btn btn-primary" type="submit">Attach</button>
    </div>
</form>
@endsection
