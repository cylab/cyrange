@extends('layouts.app')

@section('content')

<h1>Profile</h1>

<div class="card my-4">
    <div class="card-body">
        {{ $user->email }}
    </div>
</div>

<div class="card">
    <div class="card-header">2-factors authentication</div>

    <div class="card-body">
        @if ($user->has2fa())
        <p>
          2-factors authentication is
          <span class='badge bg-success'>enabled</span>
        </p>
        @else
        <p>
          2-factors authentication is currently
          <span class='badge bg-warning'>disabled</span>. To enable:
        </p>

        <ol class="list-left-align">
            <li>Open your OTP app and <b>scan the following QR-code</b>
                <p class="text-center">
                    <img src="{{ $qr_code }}">
                </p>
            </li>

            <li>Generate a One Time Password (OTP) and enter the value below.

                <form action="{{ action('ProfileController@twofa') }}" method="POST"
                      class="form-inline">
                    @csrf
                    <input name="otp" class="form-control mr-1{{ $errors->has('otp') ? ' is-invalid' : '' }}"
                           type="number" min="0" max="999999" step="1"
                           style="width: 10em; display: inline-block"
                           required autocomplete="off">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    @if ($errors->has('otp'))
                    <span class="invalid-feedback text-left">
                        <strong>{{ $errors->first('otp') }}</strong>
                    </span>
                    @endif
                </form>
            </li>
        </ol>
        @endif
    </div>
</div>


<div class="card my-4">
    <div class="card-body">
        <form method="POST"
              action="{{ action("ProfileController@update") }}">
            {{ method_field("PUT") }}
            {{ csrf_field() }}

            <div class="mb-3">
                <label for="ssh_key" class="form-label">SSH public key</label>

                <textarea id="ssh_key"
                       rows="6"
                       class="form-control{{ $errors->has('ssh_key') ? ' is-invalid' : '' }}"
                       name="ssh_key">{{ old('ssh_key', $user->ssh_key) }}</textarea>

                @if ($errors->has('old_password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('old_password') }}</strong>
                </span>
                @endif
            </div>
            
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-check"></i> Update
                </button>
            </div>
        </form>
    </div>
</div>

<div class="card my-4">
    <div class="card-header">Change password</div>

    <div class="card-body">
        <form method="POST"
              action="{{ action("ProfileController@updatePassword") }}">
            {{ method_field("PUT") }}
            {{ csrf_field() }}

            <div class="mb-3">
                <label for="old_password" class="form-label">Old password</label>

                <input id="old_password" type="password"
                       class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}"
                       name="old_password"
                       value="{{ old('old_password') }}" required>

                @if ($errors->has('old_password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('old_password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="mb-3">
                <label for="password" class="form-label">New password</label>

                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password"
                       value="{{ old('password') }}" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="mb-3">
                <label for="password_confirmation" class="form-label">
                    New password (confirm)
                </label>

                <input id="password_confirmation" type="password"
                       class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                       name="password_confirmation"
                       value="{{ old('password_confirmation') }}" required>

                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-check"></i> Update password
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
