@extends('layouts.app')

@section('content')
<h1>Template</h1>

@if (!$template->exists)
<form method="POST" action="{{ action("TemplateController@store") }}">
@else
<form method="POST"
      action="{{ action("TemplateController@update", ["template" => $template]) }}">
{{ method_field("PUT") }}
@endif
    @include("template.form")

    <div class="mb-3">
        <label for="email_note" class="form-label">Email note</label>

        <textarea id="email_note" type="text"
                  rows="10"
               class="form-control{{ $errors->has('email_note') ? ' is-invalid' : '' }}"
               name="email_note"
               placeholder="This message will be appended to the email sent to the user..."
               >{{ old('email_note', $template->email_note) }}</textarea>

        @if ($errors->has('email_note'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email_note') }}</strong>
            </span>
        @endif
    </div>

    <div class="mb-3">
        <button type="submit" class="btn btn-primary">
            <i class="fas fa-check"></i> Save
        </button>
    </div>
</form>
@endsection
