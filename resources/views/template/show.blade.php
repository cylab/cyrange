@extends('layouts.app')

@section('content')

<h1>{{ $template->name }}</h1>

<div class="mb-3">
    <a class="btn btn-primary"
       href="{{ action('TemplateController@edit', ['template' => $template]) }}">
        <i class="fas fa-edit"></i> Edit
    </a>

    <a class="btn btn-primary"
       href="{{ action('VMController@createFromTemplate', ['template' => $template]) }}">
        <i class="fas fa-cogs"></i> Deploy
    </a>

    <form method="POST"
          action="{{ action('TemplateController@destroy', ['template' => $template]) }}"
          style="display: inline-block">
        {{ csrf_field() }}
        {{ method_field("DELETE") }}
        <button class="btn btn-danger">
            <i class="fas fa-times-circle"></i> Delete
        </button>
    </form>
</div>

<p>Image: {{ $template->image->name }}</p>
<p>CPU cores: {{ $template->cpu_count }}</p>
<p>Memory: {{ $template->memory }}MB</p>

<p>
    Configure guest: 
    @if ($template->need_guest_config)
    <span class="badge badge-primary">YES</span>
    @else
    <span class="badge badge-warning">NO</span>
    @endif
</p>

<p>
    Import user SSH key: 
    @if ($template->import_ssh)
    <span class="badge badge-primary">YES</span>
    @else
    <span class="badge badge-warning">NO</span>
    @endif
</p>

<p>Provisioning commands:</p>
<code>
    <pre>{{ $template->provision }}</pre>
</code>

<p>Boot delay: {{ $template->boot_delay }} seconds</p>

<p>Email note:</p>
<code>
    <pre>{{ $template->email_note }}</pre>
</code>


@endsection
