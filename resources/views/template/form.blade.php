{{ csrf_field() }}

<div class="mb-3">
    <label for="name" class="form-label">Name</label>

    <input id="name" type="text"
           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
           name="name"
           value="{{ old('name', $template->name) }}" required autofocus>

    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="mb-3">
    <label for="image_id" class="form-label">Image</label>

    <select name="image_id" id="image_id"
            class="form-select{{ $errors->has('image') ? ' is-invalid' : '' }}">
        
        @if (! is_null($template->image))
        <option value="{{ $template->image->id  }}">{{ $template->image->name }}</option>
        @endif
        
        @foreach (\App\Image::orderBy('name')->get() as $image)
        <option value="{{ $image->id  }}">{{ $image->name }}</option>
        @endforeach
    </select>


    @if ($errors->has('image_id'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('image_id') }}</strong>
        </span>
    @endif
</div>

<div class="mb-3">
    <label for="cpu_count" class="form-label">CPU cores</label>

    <input id="cpu_count" type="number"
           class="form-control{{ $errors->has('cpu_count') ? ' is-invalid' : '' }}"
           name="cpu_count"
           value="{{ old('cpu_count', $template->cpu_count) }}" required>

    @if ($errors->has('cpu_count'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('cpu_count') }}</strong>
        </span>
    @endif
</div>

<div class="mb-3">
    <label for="memory" class="form-label">Memory [MB]</label>

    <input id="memory" type="number"
           class="form-control{{ $errors->has('memory') ? ' is-invalid' : '' }}"
           name="memory"
           value="{{ old('memory', $template->memory) }}" required >

    @if ($errors->has('memory'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('memory') }}</strong>
        </span>
    @endif
</div>

<div class="mb-3">
    <label for="need_guest_config" class="form-label">Configure guest OS</label>

    <div class="form-check">
        <input class="form-check-input" type="checkbox" id="need_guest_config" name="need_guest_config"
               {{ $template->need_guest_config ? 'checked' : '' }}>
    </div>
    <span class="text-small text-muted">Change hostname and password, and run provisioning commands (see below)</span>
</div>

<div class="mb-3">
    <label for="import_ssh" class="form-label">Import SSH key</label>

    <div class="form-check">
        <input class="form-check-input" type="checkbox" id="import_ssh" name="import_ssh"
               {{ $template->import_ssh ? 'checked' : '' }}>
    </div>
</div>

<div class="mb-3">
    <label for="provision" class="form-label">Provisioning commands</label>

    <textarea id="provision" type="text"
              rows="10"
           class="form-control{{ $errors->has('provision') ? ' is-invalid' : '' }}"
           name="provision"
           >{{ old('provision', $template->provision) }}</textarea>

    @if ($errors->has('provision'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('provision') }}</strong>
        </span>
    @endif
</div>

<div class="mb-3">
    <label for="boot_delay" class="form-label">Boot delay [seconds]</label>

    <input id="boot_delay" type="number"
           class="form-control{{ $errors->has('boot_delay') ? ' is-invalid' : '' }}"
           name="boot_delay"
           value="{{ old('boot_delay', $template->boot_delay) }}" required >

    @if ($errors->has('boot_delay'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('boot_delay') }}</strong>
        </span>
    @endif

    <span class="text-small text-muted">How long should I wait after the VM is configured (to get the IP)?</span>
</div>