@component('mail::message')
# {{ $notification->title }}

{{ $notification->markdown }}

@component('mail::subcopy')
    [{{ config('app.url') }}]({{ config('app.url') }})
@endcomponent
@endcomponent
