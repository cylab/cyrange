@component('mail::message')
# Deployed {{ $blueprint->getHostname() }}

**{{ $blueprint->getHostname() }}** was deployed on the **Cyber Range**.


IP: **{{ $blueprint->getVm()->getNetworkAdapter(0)->getIPAddress() }}**

@if ($blueprint->needGuestConfig())
login: **vagrant**

password: **{{ $blueprint->getPassword() }}**
@endif

Greetings,

cylab.be
@endcomponent
