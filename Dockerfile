#### Step 1 : composer

FROM cylab/php:7.4 AS composer

COPY . /var/www/html
WORKDIR /var/www/html
RUN composer install --no-dev --optimize-autoloader

#### Step 2 : node

FROM node:16.15.0-alpine AS node

COPY . /var/www/html
WORKDIR /var/www/html
RUN npm --version && npm install && npm run prod

#### Step 3 : the actual docker image

FROM cylab/laravel:7.4

# Install php-imagick
# required for 2FA QR code
# !! is already installed in this image !!
#RUN apt-get update && apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*
#RUN printf "\n" | pecl install imagick
#RUN docker-php-ext-enable imagick

COPY . /var/www/html
COPY ./docker/cyrange.env /var/www/html/.env

COPY --from=composer /var/www/html/vendor /var/www/html/vendor

COPY --from=node /var/www/html/public/css /var/www/html/public/css
COPY --from=node /var/www/html/public/js /var/www/html/public/js
COPY --from=node /var/www/html/public/fonts /var/www/html/public/fonts

