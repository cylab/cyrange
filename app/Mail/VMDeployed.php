<?php

namespace App\Mail;

use App\Cyrange\Blueprint;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VMDeployed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     *
     * @var \App\Cyrange\Blueprint
     */
    public $blueprint;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blueprint $blueprint)
    {
        $this->blueprint = $blueprint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->subject("[Cyber Range] Deployed " . $this->blueprint->getHostname())
                ->markdown('emails.vm.deployed');
    }
}
