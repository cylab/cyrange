<?php

namespace App;

/**
 * Helper class to display notifications with toastr.js
 * https://cylab.be/blog/219/notifications-with-toastrjs-and-laravel
 *
 * @author Thibault Debatty
 */
class Toastr
{
    private static $instance = null;

    public static function get() : Toastr
    {
        if (is_null(self::$instance)) {
            self::$instance = new Toastr();
        }

        return self::$instance;
    }

    public function info(string $msg)
    {
        session()->push('toastr-info', $msg);
    }

    public function warning(string $msg)
    {
        session()->push('toastr-warning', $msg);
    }

    public function success(string $msg)
    {
        session()->push('toastr-success', $msg);
    }

    public function error(string $msg)
    {
        session()->push('toastr-error', $msg);
    }
}
