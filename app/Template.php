<?php

namespace App;

use App\Cyrange\Blueprint;
use App\Cyrange\InterfaceBlueprint;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Template
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $name
 * @property int $cpu_count
 * @property int $memory
 * @property int $need_guest_config
 * @property string $provision
 * @property string $email_note
 * @property bool $import_ssh
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereCpuCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereEmailNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereMemory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereNeedGuestConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereProvision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $boot_delay
 * @property int $image_id
 * @property \App\Image $image
 * @method static \Illuminate\Database\Eloquent\Builder|Template whereBootDelay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Template whereImageId($value)
 */
class Template extends Model
{

    protected $dateFormat = 'U';

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
        // set default values
        $this->need_guest_config = 1;
        $this->cpu_count = 4;
        $this->memory = 4096;
        $this->boot_delay = 30;
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function getBlueprint() : Blueprint
    {
        $blueprint = new Blueprint();
        $blueprint->setCpuCap(100);
        $blueprint->setCpuCount($this->cpu_count);
        $blueprint->setGroupName("/cyrange");
        $blueprint->setHostname("cylab");
        $blueprint->setImage($this->image->getPathForVBox());
        $blueprint->setMemory($this->memory);
        $blueprint->setNeedRdp(true);
        $blueprint->setPassword(Str::random(10));

        $commands = [];
        foreach (\explode("\n", $this->provision) as $line) {
            $commands[] = trim($line);
        }

        $blueprint->setProvision($commands);
        $blueprint->setNeedGuestConfig((bool) $this->need_guest_config);

        $interface = new InterfaceBlueprint();
        $interface->setMode(InterfaceBlueprint::BRIDGED);
        $interface->network = config("cyrange.default_bridge_interface");
        $blueprint->addInterface($interface);

        return $blueprint;
    }
}
