<?php

namespace App\Http\Controllers;

use App\VBoxVM;
use App\Jobs\ImportDVD;

use Cylab\Vbox\Medium;

use Illuminate\Http\Request;

class DVDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $disks = [];

        try {
            $disks = VBoxVM::vbox()->getDVDImages();
        } catch (\SoapFault $ex) {
            toastr()->error("Failed to get list of VM's. Is VirtualBox webservice "
                . "running? " . $ex->getMessage());
        }
        return view("dvds.index", ["disks" => $disks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $vbox = VBoxVM::vbox();
        $disk = new Medium($id, $vbox);

        return view("dvds.show", ["disk" => $disk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show form to start DVD import job.
     */
    public function import()
    {
        return view("dvds.import");
    }

    public function doImport(Request $request)
    {
        $request->validate([
            "url" => "required|url",
            "name" => "required|string|min:5"]);

        $url = $request->input('url');
        $name = $request->input('name');

        $result = ImportDVD::dispatch($url, $name);
        return redirect($result->url());
    }
}
