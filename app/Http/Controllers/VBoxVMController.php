<?php
namespace App\Http\Controllers;

use App\VBoxVM;
use App\VM;
use App\Jobs\HaltAll;
use App\Jobs\UpAll;

use Cylab\Vbox\NetworkAdapter;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VBoxVMController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $vms = [];

        try {
            $vms = VBoxVM::all();
        } catch (\SoapFault $ex) {
            toastr()->error("Failed to get list of VM's. Is VirtualBox webservice "
                . "running? " . $ex->getMessage());
        }
        return view("vbox.index", ["vms" => $vms]);
    }

    public function assign(string $uuid)
    {
        $vboxvm = VBoxVM::find($uuid);

        $vm = new VM();
        $vm->name = $vboxvm->getName();
        $vm->uuid = $uuid;
        $vm->user_id = Auth::user()->id;
        $vm->save();

        return redirect($vm->url());
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(string $uuid)
    {
        $vm = VBoxVM::find($uuid);
        if ($vm->isRunning()) {
            toastr()->error('VM must be powered off!');
            return redirect(action('VMController@show', ['vm' => VM::findByUUID($vm->getUUID())]));
        }

        return view("vbox.edit", ["vboxvm" => $vm]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     */
    public function update(Request $request, string $uuid)
    {
        $request->validate([
            // name cannot be modified
            // we only allow to modify the cyberrange-level name of the machine
            // !! virtualbox allows max 32 vCPU's per VM
            'cpu_count' => 'required|integer|min:1|max:32',
            'cpu_cap' => 'required|integer|min:1|max:100',
            'memory' => 'required|integer|min:64|max:131072'
        ]);

        try {
            $vboxvm = VBoxVM::find($uuid);
            $vboxvm->setCPUCount($request->cpu_count);
            $vboxvm->setMemorySize($request->memory);
            $vboxvm->setCPUCap($request->cpu_cap);
        } catch (\SoapFault $ex) {
            toastr()->error("Failed to modify this VM : " . $ex->getMessage());
            return redirect()->back()->withInput();
        }

        toastr()->info('VM updated');
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function editNetwork(string $uuid, int $slot)
    {
        $vm = VBoxVM::find($uuid);

        if ($vm->isRunning()) {
            toastr()->warning("Machine must be off to modify network adapters!");
            return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
        }

        $interface = $vm->getNetworkAdapter($slot);
        $host_interfaces = VBoxVM::hostInterfaces();
        $internal_networks = VBoxVM::vbox()->getDHCPServers();
        return view("vbox.network", [
            "vboxvm" => $vm,
            "slot" => $slot,
            "interface" => $interface,
            "host_interfaces" => $host_interfaces,
            "internal_networks" => $internal_networks]);
    }

    public function updateNetwork(Request $request, string $uuid, int $slot)
    {
        $vboxvm = VBoxVM::find($uuid);
        $interface = $vboxvm->getNetworkAdapter($slot);
        $interface->setEnabled($request->input("enabled", false));

        if ($request->input('mode') == "Bridged") {
            $interface->setAttachmentType(NetworkAdapter::ATTACHEMENT_BRIDGED);
            $interface->setBridgedInterface($request->input("bridge", ""));
        } else {
            $interface->setAttachmentType(NetworkAdapter::ATTACHEMENT_INTERNAL);
            $interface->setInternalNetwork($request->input('internal'));
        }

        toastr()->info('Network interface updated');
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function reset(string $uuid)
    {
        try {
            VBoxVM::find($uuid)->reset();
        } catch (\SoapFault $ex) {
            toastr()->error(
                "Failed to reset this VM : " . $ex->getMessage()
            );
            return redirect()->back();
        }
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function up(string $uuid)
    {
        try {
            VBoxVM::find($uuid)->up();
        } catch (\SoapFault $ex) {
            toastr()->error(
                "Failed to start this VM : " . $ex->getMessage()
            );
            return redirect()->back();
        }
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function halt(string $uuid)
    {
        try {
            VBoxVM::find($uuid)->halt();
        } catch (\SoapFault $ex) {
            toastr()->error(
                "Failed to halt this VM : " . $ex->getMessage()
            );
            return redirect()->back();
        }
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function kill(string $uuid)
    {
        try {
            VBoxVM::find($uuid)->kill();
        } catch (\SoapFault $ex) {
            toastr()->error(
                "Failed to kill this VM : " . $ex->getMessage()
            );
            return redirect()->back();
        }
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function haltAll()
    {
        $result = HaltAll::dispatch();
        return redirect(action('JobController@show', ['job' => $result]));
    }

    public function upAll()
    {
        $result = UpAll::dispatch();
        return redirect(action('JobController@show', ['job' => $result]));
    }

    /**
     * Show form to create and assign a new virtual disk.
     *
     * @param string $uuid
     * @param string $controller
     */
    public function createDisk(string $uuid, string $controller)
    {
        return view("vm.storage.edit", ["uuid" => $uuid, "controller" => $controller]);
    }

    /**
     * Create and assign a new virtual disk.
     *
     * @param string $uuid
     * @param string $controller
     * @param Request $request
     */
    public function storeDisk(string $uuid, string $controller, Request $request)
    {
        $request->validate([
            "name" => "required|string|min:5",
            "size" => "required|int|min:1"
        ]);

        $vbox = VBoxVM::vbox();
        $vm = $vbox->findVM($uuid);

        $size = $request->get("size") * 1024 * 1024 * 1024;
        $location = $vm->getSettingsDirectory() . "/" . $request->get("name") . ".vdi";

        try {
            $medium = $vbox->createHardDisk($location, $size);
        } catch (\SoapFault $ex) {
            toastr()->error(
                "Failed to create disk : " . $ex->getMessage()
            );
            return redirect()->back();
        }

        $vm->getStorageControllerByName($controller)->attachDevice($medium);

        toastr()->success("New disk created!");

        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    public function detachDisk(string $uuid, string $controller, int $port, int $device)
    {
        $vbox = VBoxVM::vbox();
        $vm = $vbox->findVM($uuid);

        if ($vm->isRunning()) {
            toastr()->warning("VM must be powered off!");
            return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
        }

        $storage_controller = $vm->getStorageControllerByName($controller);
        $storage_controller->detachDevice($port, $device);

        toastr()->success("Disk detached");

        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }

    /**
     * Show form to attach a DVD.
     *
     * @param string $uuid
     * @param string $controller
     */
    public function attachDVD(string $uuid, string $controller)
    {
        return view("vm.storage.attach", [
            "uuid" => $uuid,
            "controller" => $controller,
            "disks" => vbox()->getDVDImages()]);
    }

    /**
     * Detach a DVD.
     *
     * @param string $uuid
     * @param string $controller
     * @param Request $request
     */
    public function doAttachDVD(string $uuid, string $controller, Request $request)
    {
        $vm = vbox()->findVM($uuid);

        if ($vm->isRunning()) {
            toastr()->warning("VM must be powered off!");
            return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
        }

        $medium = new \Cylab\Vbox\Medium($request->get("uuid"), vbox());
        $storage_controller = $vm->getStorageControllerByName($controller);
        $storage_controller->attachDevice($medium);

        toastr()->success("Drive attached");
        return redirect(action('VMController@show', ['vm' => VM::findByUUID($uuid)]));
    }
}
