<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use PragmaRX\Google2FAQRCode\Google2FA;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $user = Auth::user();

        // generate a secret
        $google2fa = new Google2FA();
        $secret = $google2fa->generateSecretKey(64);

        // generate the QR code, indicating the address
        // of the web application and the user name
        // or email in this case
        $qr_code = $google2fa->getQRCodeInline(
            config("app.name"),
            $user->email,
            $secret
        );

        // store the current secret in the session
        // will be used when we enable 2FA (see below)
        session([ "2fa_secret" => $secret]);

        return view('profile.edit', [
            'user' => $user,
            "qr_code" => $qr_code]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->ssh_key = $request->ssh_key;
        $user->save();
        toastr()->success('Profile updated!');
        return redirect(action('ProfileController@edit'));
    }


    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|min:8',
            'old_password' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail($attribute . ' is invalid.');
                    }
                },
            ]
        ]);

        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        toastr()->success('Password updated!');

        // change password for guacamole user
        $guacamole = $user->guacamole();
        if ($guacamole != null) {
            $guacamole->setPassword($request->password);
            $guacamole->save();
            toastr()->success('Password updated for Guacamole account!');
        }

        return redirect(action('ProfileController@edit'));
    }


    /**
     * check the submitted OTP
     * if correct, enable 2FA
     */
    public function twofa(Request $request)
    {
        $google2fa = new Google2FA();

        // retrieve secret from the session
        $secret = session("2fa_secret");
        $user = Auth::user();

        if ($google2fa->verify($request->input('otp'), $secret)) {
            // store the secret in the user profile
            // this will enable 2FA for this user
            $user->twofa_secret = $secret;
            $user->save();

            // avoid double OTP check
            session(["2fa_checked" => true]);

            return redirect(action('ProfileController@edit'));
        }

        throw ValidationException::withMessages([
            'otp' => 'Incorrect value. Please try again...']);
    }
}
