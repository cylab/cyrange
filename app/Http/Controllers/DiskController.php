<?php

namespace App\Http\Controllers;

use App\VBoxVM;

use Cylab\Vbox\Medium;

use Illuminate\Http\Request;

class DiskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $disks = [];

        try {
            $disks = VBoxVM::vbox()->getHarddisks();
        } catch (\SoapFault $ex) {
            toastr()->error("Failed to get list of VM's. Is VirtualBox webservice "
                . "running? " . $ex->getMessage());
        }
        return view("disks.index", ["disks" => $disks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $vbox = VBoxVM::vbox();
        $disk = new Medium($id, $vbox);

        return view("disks.show", ["disk" => $disk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $vbox = VBoxVM::vbox();
        $disk = new Medium($id, $vbox);
        $disk->delete();

        toastr()->info("Disk destroyed");

        return(redirect(route("disks.index")));
    }
}
