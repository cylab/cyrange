<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Jobs\SendNotification;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view("notification.index", ["notifications" => Notification::all()->sortByDesc("created_at")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view("notification.edit", ["notification" => new Notification]);
    }

    public function save(Notification $notification, Request $request)
    {
        $request->validate([
            "title" => "required|string|max:255",
            "markdown" => "required|string"
        ]);

        $notification->title = $request->get("title");
        $notification->markdown = $request->get("markdown");
        $notification->user_id = auth()->id();
        $notification->save();

        SendNotification::dispatch($notification);

        return redirect(route("notifications.index"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->save(new Notification, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  Notification $notification
     */
    public function show(Notification $notification)
    {
        return view("notification.show", ["notification" => $notification]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }
}
