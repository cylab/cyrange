<?php

use App\VBoxVM;
use Cylab\Vbox\VBox;

function vbox() : VBox
{
    return VBoxVM::vbox();
}

use League\CommonMark\GithubFlavoredMarkdownConverter;

/**
 * Convert markdown to html
 * @param string $markdown
 * @return string
 */
function markdown(string $markdown) : string
{
    $converter = new GithubFlavoredMarkdownConverter([
        'html_input' => 'strip',
        'allow_unsafe_links' => false,
    ]);

    return $converter->convertToHtml($markdown);
}

use App\Toastr;

function toastr() : Toastr
{
    return Toastr::get();
}
