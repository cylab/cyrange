<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $title
 * @property string $markdown
 * @property User $user
 */
class Notification extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function html() : string
    {
        return markdown($this->markdown);
    }
}
