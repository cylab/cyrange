<?php

namespace App\Jobs;

use App\VM;
use App\JobResult;

class UpAll extends JobWithLog
{

    protected function doHandle()
    {
        foreach (VM::all() as $vm) {
            /** @var VM $vm */
            $this->logger()->notice("Starting " . $vm->name . " ...");
            $vm->getVBoxVM()->up();
            sleep(5);
        }
    }

    public function createJobResultInstance(): JobResult
    {
        return new JobResult();
    }
}
