<?php

namespace App\Jobs;

use App\VM;
use App\JobResult;

class HaltAll extends JobWithLog
{

    protected function doHandle()
    {
        foreach (VM::all() as $vm) {
            /** @var VM $vm */
            $this->logger()->notice("Stopping " . $vm->name . " ...");
            $vm->getVBoxVM()->halt();
            sleep(5);
        }
    }

    public function createJobResultInstance(): JobResult
    {
        return new JobResult();
    }
}
