<?php

namespace App\Jobs;

use App\JobResult;
use App\Downloader;

class ImportDVD extends JobWithLog
{

    private $url;
    private $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        string $url,
        string $name
    ) {
        $this->url = $url;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    protected function doHandle()
    {
        $logger = $this->result()->logger();

        $local_path = $this->localPathOf($this->name);
        if (file_exists($local_path)) {
            $logger->error("File already exists: " . $local_path);
            return;
        }

        $logger->info("Downloading from " . $this->url . " ...");

        $downloader = new Downloader($logger);
        $downloader->downloadToFile($this->url, $local_path);

        vbox()->openDVD($this->vboxPathOf($this->name));
    }

    public function createJobResultInstance(): JobResult
    {
        $result = new JobResult();
        $result->name = $this->name;
        return $result;
    }

    /**
     * Get the path on disk as seen by the web application and job processing code (hence INSIDE the docker container)
     * @param string $name
     * @return string
     */
    private function localPathOf(string $name) : string
    {
        if (substr($name, -4) != ".iso") {
            $name .= ".iso";
        }

        $directory = storage_path("app/dvds");
        if (! is_dir($directory)) {
            mkdir($directory);
        }

        return $directory . "/" . $name;
    }

    /**
     * Get the path on disk as seen by the VirtualBox server (hence OUTSIDE the docker container).
     * @return string
     */
    private function vboxPathOf(string $name) : string
    {
        if (substr($name, -4) != ".iso") {
            $name .= ".iso";
        }

        return config('vbox.root') . "/dvds/" . $name;
    }
}
