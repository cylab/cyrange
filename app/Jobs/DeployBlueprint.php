<?php

namespace App\Jobs;

use App\User;
use App\VM;
use App\JobResult;
use App\Guacamole;

use App\Mail\VMDeployed;

use App\Cyrange\Blueprint;
use App\Cyrange\BlueprintDeployer;

use Cylab\Vbox\VBox;

use Illuminate\Support\Facades\Mail;

class DeployBlueprint extends JobWithLog
{
    private $blueprint;
    private $user;
    private $guacamole_email;
    private $boot_delay = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Blueprint $blueprint, User $user, ?string $guacamole_email, ?int $boot_delay = 0)
    {
        $this->blueprint = $blueprint;
        $this->user = $user;
        $this->guacamole_email = $guacamole_email;
        $this->boot_delay = $boot_delay;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    protected function doHandle()
    {
        $result = $this->result();
        $logger = $result->logger();

        $vbox = new VBox(
            config('vbox.user'),
            config('vbox.password'),
            "http://" . config('vbox.host') . ":18083"
        );

        $deployer = new BlueprintDeployer($vbox, $logger);
        $vboxvm = $deployer->deploy($this->blueprint);

        $vm = new VM();
        $vm->user_id = $this->user->id;
        $vm->name = $this->blueprint->getHostname();
        $vm->uuid = $vboxvm->getUUID();
        $vm->save();

        $logger->info("Wait maximum " . $this->boot_delay . " seconds for VM to boot...");
        $max_time = time() + $this->boot_delay;
        while (time() < $max_time) {
            sleep(5);
            if ($vboxvm->getNetworkAdapter(0)->getIPAddress() != "") {
                break;
            }
        }
        $logger->info("IP : " . $vboxvm->getNetworkAdapter(0)->getIPAddress());

        // Create guacamole connection and user if needed
        if ($this->guacamole_email != null) {
            $logger->info("Create web access for user " . $this->guacamole_email);
            Guacamole::assignVMToEmail($this->blueprint, $this->guacamole_email);
        }

        // Notify owner
        Mail::to($this->user->email)->send(
            new VMDeployed($this->blueprint)
        );
    }

    public function createJobResultInstance(): JobResult
    {
        $result = new JobResult();
        $result->name = $this->blueprint->getHostname();
        return $result;
    }
}
